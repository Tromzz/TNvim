-- This file can be loaded by calling `lua require('plugins')` from your init.vim
-- Install packer
--local install_path = vim.fn.stdpath 'data' .. '/site/pack/packer/start/packer.nvim'
--local is_bootstrap = false
--if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
--is_bootstrap = true
--vim.fn.system {'git','clone','--depth','1','https://github.com/wbthomason/packer.nvim',install_path }
--vim.cmd [[packadd packer.nvim]]
--end
-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  use {
  'nvim-telescope/telescope.nvim', tag = '0.1.4',
-- or                            , branch = '0.1.x',
  requires = { {'nvim-lua/plenary.nvim'} }
}

  use({
	  'rose-pine/neovim',
	  as = 'rose-pine' ,
	  config = function()
		  vim.cmd('colorscheme rose-pine')
	  end })

	use('nvim-treesitter/nvim-treesitter', {run = ':TSUpdate'})
	use('ThePrimeagen/harpoon')
	use('mbbill/undotree')
	use('tpope/vim-fugitive')
	use("nvim-treesitter/nvim-treesitter-context");


use {
  'VonHeikemen/lsp-zero.nvim',
  branch = 'v3.x',
  requires = {
    --- Uncomment these if you want to manage LSP servers from neovim
    {'williamboman/mason.nvim'},
    {'williamboman/mason-lspconfig.nvim'},
	
    -- LSP Support
    {'neovim/nvim-lspconfig'},
    -- Autocompletion
	{'hrsh7th/cmp-buffer'},
	{'hrsh7th/cmp-path'},
	{'saadparwaiz1/cmp_luasnip'},
	{'hrsh7th/cmp-nvim-lua'},
    {'hrsh7th/nvim-cmp'},
    {'hrsh7th/cmp-nvim-lsp'},
    {'L3MON4D3/LuaSnip'},
	{'rafamadriz/friendly-snippets'},
  }
}

 use("eandrju/cellular-automaton.nvim")
 use("folke/zen-mode.nvim")
--use {
  --'nvim-tree/nvim-tree.lua',
  --requires = {
    --'nvim-tree/nvim-web-devicons', -- optional
 -- },
--}
use {
	"windwp/nvim-autopairs",
    config = function() require("nvim-autopairs").setup {} end
}

--Git
	

end)
