-- Install packer
local install_path = vim.fn.stdpath 'data' .. '/site/pack/packer/start/packer.nvim'
local is_bootstrap = false
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
is_bootstrap = true
vim.fn.system {'git','clone','--depth','1','https://github.com/wbthomason/packer.nvim',install_path }
vim.cmd [[packadd packer.nvim]]
end
require("Tromzz.remap")
vim.opt.mouse = 'v'
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.autoindent = true
vim.opt.ignorecase = true
vim.opt.hlsearch = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.showmatch = true
vim.opt.ttyfast = true
vim.opt.incsearch = true
vim.opt.scrolloff = 8

