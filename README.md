# My Neovim Config

## Requirements

- [Neovim](https://neovim.io/) 0.9.0+

## Instructions

clone into $HOME/.config/nvim

```
git clone --depth 1 https://codeberg.org/Tromzz/TNvim.git $HOME/.config/nvim
```
- Open Neovim and wait for the installation of plugins to finish.

To get started, first clone this repository to somewhere on your packpath, e.g.:

Unix, Linux Installation
```
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```

If you use Arch Linux, there is also an [AUR package](https://aur.archlinux.org/packages/nvim-packer-git).

Windows Powershell Installation

```
  git clone https://github.com/wbthomason/packer.nvim "$env:LOCALAPPDATA\nvim-data\site\pack\packer\start\packer.nvim"
 
```

Installing build-essential package in Ubuntu is as simple as typing this command in the terminal:

```
sudo apt update && sudo apt install build-essential
```
**Removing build-essential tool from Ubuntu**

Keeping those development tools won’t harm your system. but if you are running low on the disk space, you may consider removing it.

Removing software is easy in Ubuntu thanks to the apt remove command:

```
sudo apt remove build-essential
```

It will be a good idea to run the autoremove command to remove the residual dependency packages as well:

```
sudo apt autoremove
```
